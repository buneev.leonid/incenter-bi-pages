# incenter-bi-pages

This repository is as a storage mechanism for user-created dashboards.

Git was used instead of regular database because we want to store history of all changes to our pages.

Idea is that users will commit directly to this repository from `incenter-bi` via Git REST API. However, developers also may make changes in it.

Please do not create any files outside `reports`, `dashboards` and `spreadsheets` folders.

Code is downloaded (again via REST API) and executed by incetner-bi at **RUNTIME**. In other words, 
files in this repository do not participate in incenter-bi build whatsoever.

## IDE tips

### VSCode workspace

Calm workspace from `tyrecheck-monorepo` is set up in such a way that it references `incenter-bi-pages`, but 
for it to work correctly you need to clone `incenter-bi-pages` into the same directory that `tyrecheck-monorepo` is 
in. In other words, file structure may look like this:

```
/Work
  /incenter-bi-pages
    /dashboards
      /MyDashboard.vue
  /tyrecheck-monorepo
    /packages
      /incenter-bi
      ...
    /package.json
```

You may need to reload VSCode after cloning the repo (`Command palette => "reload window"`)

### VSCode - git

If your worksapce is set up correctly, you will see 2 repositories opened at the same time. They may be on 
different branches (and they will be - `incenter-bi-pages` doesn't use merge requests, and all commits go 
directly to `master`).

This is useful, but a little bit annoying because you need to choose which repo you're working in when 
doing `Git: Checkout` and other git operations.

If you plan to work only in 1 repo, you may click right mouse btn on the repo name in "Source control" 
section of VSCode and close repo. Do not worry, it's just cosmetic change, if you make some changes in 
files from this repo it will be "reopened".

## Security concerns

At the moment, anyone can commit into the repo. Yes, it is huge security concern that wasn't yet fixed due to
time constraints. Ideally, user should login with azure credentials, and Git should somehow respect same azure 
credentials to allow user to push code.

## Supported JS

As code is evaluated directly by the browser without compiling, please do not use those features:

- `?.` and `??` operators
- `import`. Imports are processed by webpack during build, so they cannot be used during runtime. 
  Instead, you can add import in `incenter-bi`'s component (e.g. `src/pages/dashboards/UserDefinedDashboard.vue`).
  Then you can reference imported package from `incenter-bi-pages`.
- options `components`, `props`, `mounted()`, `created()`, `beforeDestroy()` will have no effect.